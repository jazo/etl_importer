EtlImporter::Engine.routes.draw do
  devise_for :admin_users, class_name: 'EtlImporter::AdminUser'
  ActiveAdmin.routes(self)
end
