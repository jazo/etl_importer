module EtlImporter
  class Engine < ::Rails::Engine
    # isolate_namespace EtlImporter
    #
    config.generators do |g|
      g.test_framework :rspec, fixture: true
      g.fixture_replacement :factory_bot, dir: 'spec/factories'
      g.view_specs false
      g.helper_specs false
      g.stylesheets = false
      g.javascripts = false
      g.helper = false
      g.orm :active_record, primary_key_type: :uuid
    end
  end
end
