require 'activeadmin'
require 'etl_importer/engine'

module EtlImporter
  def self.table_name_prefix
    'etl_importer_'
  end
end
