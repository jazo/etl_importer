$:.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'etl_importer/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = 'etl_importer'
  spec.version     = EtlImporter::VERSION
  spec.authors     = ['Jazo Madrid']
  spec.email       = ['jazo@appio.mx']
  spec.summary     = 'A ETL importer for aelogic test.'
  spec.description = 'A ETL importer for aelogic test.'
  spec.license     = 'MIT'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = 'TODO: Set to "http://mygemserver.com"'
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  spec.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  spec.add_dependency 'activeadmin'
  spec.add_dependency 'activeadmin_addons'
  spec.add_dependency 'light-service', '~> 0.11'
  spec.add_dependency 'rails', '~> 5.2.3'
  spec.add_dependency 'sass-rails'

  spec.add_development_dependency 'annotate'
  spec.add_development_dependency 'kramdown'
  spec.add_development_dependency 'pg'
  spec.add_development_dependency 'rails-erd'
  spec.add_development_dependency 'rspec-rails'
  spec.add_development_dependency 'simplecov'
end
