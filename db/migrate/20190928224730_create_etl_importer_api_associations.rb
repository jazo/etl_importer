class CreateEtlImporterApiAssociations < ActiveRecord::Migration[5.2]
  def change
    create_table :etl_importer_api_associations, id: :uuid do |t|
      t.references :etl_importer_discount_plan, foreign_key: true, type: :uuid, index: {name: :index_etl_importer_api_associations_on_etl_importer_d_plan_id}

      t.timestamps
    end
  end
end
