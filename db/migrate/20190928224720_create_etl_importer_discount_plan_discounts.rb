class CreateEtlImporterDiscountPlanDiscounts < ActiveRecord::Migration[5.2]
  def change
    create_table :etl_importer_discount_plan_discounts, id: :uuid do |t|
      t.references :etl_importer_discount_plan, foreign_key: true, type: :uuid, index: {name: :index_etl_importer_d_plan_discounts_on_etl_importer_d_plan_id}
      t.string :discount_type
      t.integer :month_number
      t.decimal :amount
      t.decimal :minimum_amount
      t.decimal :maximum_amount

      t.timestamps
    end
  end
end
