class CreateEtlImporterDiscountPlanClientApplications < ActiveRecord::Migration[5.2]
  def change
    create_table :etl_importer_discount_plan_client_applications, id: :uuid do |t|
      t.references :etl_importer_discount_plan, foreign_key: true, type: :uuid, index: {name: :index_etl_importer_d_plan_client_app_on_etl_importer_d_plan_id}
      t.references :etl_importer_client_application, foreign_key: true, type: :uuid, index: {name: :index_etl_importer_d_pln_cli_apps_on_etl_importer_client_app_id}

      t.timestamps
    end
  end
end
