class AddDiscountPlanControlsToUnitAmenity < ActiveRecord::Migration[5.2]
  def change
    add_reference :etl_importer_unit_amenities, :etl_importer_discount_plan_control, foreign_key: true, type: :uuid, index: {name: :index_etl_importer_unit_ameniti_on_etl_importer_di_plan_cont_id}
  end
end
