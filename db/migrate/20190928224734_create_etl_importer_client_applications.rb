class CreateEtlImporterClientApplications < ActiveRecord::Migration[5.2]
  def change
    create_table :etl_importer_client_applications, id: :uuid do |t|
      t.references :etl_importer_discount_plan, foreign_key: true, type: :uuid, index: {name: :index_etl_importer_client_applicas_on_etl_importer_d_plan_id}
      t.string :name
      t.boolean :channel_rates_on
      t.boolean :internal

      t.timestamps
    end
  end
end
