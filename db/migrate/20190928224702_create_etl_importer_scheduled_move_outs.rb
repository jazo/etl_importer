class CreateEtlImporterScheduledMoveOuts < ActiveRecord::Migration[5.2]
  def change
    create_table :etl_importer_scheduled_move_outs, id: :uuid do |t|

      t.timestamps
    end
  end
end
