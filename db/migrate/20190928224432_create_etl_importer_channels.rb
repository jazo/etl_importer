class CreateEtlImporterChannels < ActiveRecord::Migration[5.2]
  def change
    create_table :etl_importer_channels, id: :uuid do |t|

      t.timestamps
    end
  end
end
