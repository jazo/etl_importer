class CreateEtlImporterTenantAccountKinds < ActiveRecord::Migration[5.2]
  def change
    create_table :etl_importer_tenant_account_kinds, id: :uuid do |t|

      t.timestamps
    end
  end
end
