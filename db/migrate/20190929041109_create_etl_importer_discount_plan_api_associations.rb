class CreateEtlImporterDiscountPlanApiAssociations < ActiveRecord::Migration[5.2]
  def change
    create_table :etl_importer_discount_plan_api_associations, id: :uuid do |t|
      t.references :etl_importer_discount_plan, foreign_key: true, type: :uuid, index: {name: :index_etl_importer_d_plan_api_assoc_on_etl_importer_d_plan_id}
      t.references :etl_importer_api_association, foreign_key: true, type: :uuid, index: {name: :index_etl_importer_d_plan_api_assoc_on_etl_importer_api_asso_id}

      t.timestamps
    end
  end
end
