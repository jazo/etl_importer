class CreateEtlImporterDiscountPlanFacilities < ActiveRecord::Migration[5.2]
  def change
    create_table :etl_importer_discount_plan_facilities, id: :uuid do |t|
      t.references :etl_importer_discount_plan, foreign_key: true, type: :uuid, index: {name: :index_etl_importer_d_plan_facilities_on_etl_importer_d_plan_id}
      t.references :etl_importer_facility, foreign_key: true, type: :uuid, index: {name: :index_etl_importer_d_plan_facilites_on_etl_importer_facility_id}

      t.timestamps
    end
  end
end
