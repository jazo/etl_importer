class CreateEtlImporterFacilities < ActiveRecord::Migration[5.2]
  def change
    create_table :etl_importer_facilities, id: :uuid do |t|
      t.references :etl_importer_discount_plan, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
