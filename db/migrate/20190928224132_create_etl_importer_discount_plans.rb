class CreateEtlImporterDiscountPlans < ActiveRecord::Migration[5.2]
  def change
    create_table :etl_importer_discount_plans, id: :uuid do |t|
      t.string :name
      t.text :description
      t.text :public_description
      t.text :availability_text
      t.text :requirements_text
      t.string :kind
      t.boolean :min_occupancy_required
      t.boolean :min_occupancy_months
      t.boolean :prepay_required
      t.integer :prepay_months
      t.date :start_date
      t.date :end_date
      t.boolean :auto_apply
      t.string :promotion_type
      t.boolean :deleted
      t.boolean :turned_on
      t.boolean :available_for_all_facilities
      t.string :round_to_nearest
      t.boolean :hide_from_website
      t.boolean :move_in_only
      t.boolean :existing_tenant_only
      t.integer :priority
      t.string :role_permission
      t.references :etl_importer_always_discount_plan_discount, foreign_key: true, type: :uuid, index: {name: :index_etl_importer_d_plan_on_etl_importer_always_dis_id}
      t.references :etl_importer_tenant_account_kind, foreign_key: true, type: :uuid, index: {name: :index_etl_importer_d_plan_on_etl_importer_tenant_acco_kind_id}
      # t.references :etl_importer_unit_group, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
