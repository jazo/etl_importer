class CreateEtlImporterInvoiceableFees < ActiveRecord::Migration[5.2]
  def change
    create_table :etl_importer_invoiceable_fees, id: :uuid do |t|
      t.string :product_code
      t.string :kind
      t.string :description
      t.boolean :required_at_move_in
      t.boolean :required_at_transfer
      t.decimal :amount
      t.text :short_description
      t.boolean :show_in_sales_center
      t.decimal :tax_total
      t.decimal :total
      t.references :etl_importer_unit_group, foreign_key: true, type: :uuid, index: {name: :index_etl_importer_invoiceable_fees_on_etl_importer_unit_group}

      t.timestamps
    end
  end
end
