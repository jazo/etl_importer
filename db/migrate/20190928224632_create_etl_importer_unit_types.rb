class CreateEtlImporterUnitTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :etl_importer_unit_types, id: :uuid do |t|
      t.string :name
      t.boolean :deleted
      t.string :internal_account_code
      t.text :code_and_description

      t.timestamps
    end
  end
end
