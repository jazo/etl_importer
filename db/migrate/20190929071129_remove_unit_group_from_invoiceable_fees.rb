class RemoveUnitGroupFromInvoiceableFees < ActiveRecord::Migration[5.2]
  def change
    remove_reference :etl_importer_invoiceable_fees, :etl_importer_unit_group
  end
end
