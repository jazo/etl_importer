class CreateEtlImporterDiscountPlanControls < ActiveRecord::Migration[5.2]
  def change
    create_table :etl_importer_discount_plan_controls, id: :uuid do |t|
      t.string :kind
      t.decimal :min_value
      t.decimal :max_value
      t.boolean :deleted
      t.date :deleted_on
      t.text :requirements_text
      t.integer :applicable_discount_plans_count
      t.references :etl_importer_unit_amenity, foreign_key: true, type: :uuid, index: {name: :index_etl_importer_d_plan_cont_on_etl_importer_unit_amenity_id}
      t.references :etl_importer_unit_type, foreign_key: true, type: :uuid, index: {name: :index_etl_importer_d_plan_controls_on_etl_importer_unit_type_id}

      t.timestamps
    end
  end
end
