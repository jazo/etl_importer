class CreateEtlImporterAlwaysDiscountPlanDiscounts < ActiveRecord::Migration[5.2]
  def change
    create_table :etl_importer_always_discount_plan_discounts, id: :uuid do |t|

      t.timestamps
    end
  end
end
