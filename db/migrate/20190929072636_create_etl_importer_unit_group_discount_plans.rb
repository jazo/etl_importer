class CreateEtlImporterUnitGroupDiscountPlans < ActiveRecord::Migration[5.2]
  def change
    create_table :etl_importer_unit_group_discount_plans, id: :uuid do |t|
      t.references :etl_importer_unit_group, foreign_key: true, type: :string, index: {name: :index_etl_importer_un_gro_disc_plans_on_etl_importer_un_gro_id}
      t.references :etl_importer_discount_plan, foreign_key: true, type: :uuid, index: {name: :index_etl_importer_un_gro_disc_plans_on_etl_importer_di_plan_id}

      t.timestamps
    end
  end
end
