class RemoveDiscountPlanFromClientApplication < ActiveRecord::Migration[5.2]
  def change
    remove_reference :etl_importer_client_applications, :etl_importer_discount_plan
  end
end
