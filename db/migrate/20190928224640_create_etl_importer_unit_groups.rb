class CreateEtlImporterUnitGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :etl_importer_unit_groups, id: :uuid do |t|
      t.decimal :price
      t.string :group_key
      t.string :name
      t.integer :available_units_count
      t.integer :total_units_count
      t.integer :total_non_excluded_units_count
      t.string :size
      t.integer :standard_rate
      t.integer :floor
      t.decimal :reduced_price
      t.integer :occupancy_percent
      t.integer :area
      t.integer :length
      t.integer :width
      t.integer :height
      t.integer :due_at_move_in
      t.integer :due_at_move_in_without_fees
      t.integer :due_monthly
      t.text :attribute_description
      t.text :description
      t.integer :average_rent
      t.string :active_rate_type
      t.references :etl_importer_channel_rate, foreign_key: true, type: :uuid
      t.references :etl_importer_unit_type, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
