class CreateEtlImporterChannelRates < ActiveRecord::Migration[5.2]
  def change
    create_table :etl_importer_channel_rates, id: :uuid do |t|
      t.string :base_rate_type
      t.string :modifier_type
      t.boolean :turned_on
      t.boolean :turned_off_on
      t.decimal :rate
      t.decimal :amount
      t.string :channel_name
      t.references :etl_importer_channel, foreign_key: true, type: :uuid
      t.references :etl_importer_facility, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
