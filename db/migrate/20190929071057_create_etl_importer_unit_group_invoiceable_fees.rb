class CreateEtlImporterUnitGroupInvoiceableFees < ActiveRecord::Migration[5.2]
  def change
    create_table :etl_importer_unit_group_invoiceable_fees, id: :uuid do |t|
      t.references :etl_importer_unit_group, foreign_key: true, type: :string, index: {name: :index_etl_importer_un_gpo_invo_fees_on_etl_importer_un_gpo_id}
      t.references :etl_importer_invoiceable_fees, foreign_key: true, type: :uuid, index: {name: :index_etl_importer_un_gpo_inv_fees_on_etl_importer_inv_fees_id}

      t.timestamps
    end
  end
end
