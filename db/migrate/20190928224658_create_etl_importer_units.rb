class CreateEtlImporterUnits < ActiveRecord::Migration[5.2]
  def change
    create_table :etl_importer_units, id: :uuid do |t|
      t.decimal :price
      t.string :name
      t.text :description
      t.integer :width
      t.integer :length
      t.integer :height
      t.integer :door_height
      t.integer :door_width
      t.string :door_type
      t.string :access_type
      t.string :floor
      t.string :size
      t.integer :area
      t.integer :standard_rate
      t.string :managed_rate
      t.boolean :available_for_move_in
      t.boolean :rentable
      t.string :status
      t.string :payment_status
      t.string :current_ledger_id
      t.string :current_tenant_id
      t.string :combination_lock_number
      t.string :attribute_description
      t.boolean :deleted
      t.boolean :damaged
      t.boolean :complimentary
      t.references :etl_importer_channel_rate, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
