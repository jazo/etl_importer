class CreateEtlImporterUnitGroupUnitAmenities < ActiveRecord::Migration[5.2]
  def change
    create_table :etl_importer_unit_group_unit_amenities, id: :uuid do |t|
      t.references :etl_importer_unit_amenities, foreign_key: true, type: :uuid, index: {name: :index_etl_importer_unit_gop_un_amen_on_etl_importer_un_amen_id}

      t.timestamps
    end

    add_reference :etl_importer_unit_group_unit_amenities, :etl_importer_unit_group,foreign_key: true, type: :string, index: {name: :index_etl_importer_uni_group_un_amen_on_etl_importer_un_gop_id}

  end
end
