class CreateEtlImporterUnitAmenities < ActiveRecord::Migration[5.2]
  def change
    create_table :etl_importer_unit_amenities, id: :uuid do |t|
      t.string :name
      t.string :short_code
      t.boolean :show_in_sales_center_filter_dropdown
      t.references :etl_importer_unit_group, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
