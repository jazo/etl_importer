class ChengeUnitGroupIdFromUuidToString < ActiveRecord::Migration[5.2]
  def change
    remove_reference :etl_importer_invoiceable_fees, :etl_importer_unit_group
    remove_reference :etl_importer_unit_amenities, :etl_importer_unit_group

    change_table :etl_importer_unit_groups do |t|
      t.remove :id
    end

    add_column :etl_importer_unit_groups, :id, :string, primary_key: true

    add_reference :etl_importer_invoiceable_fees, :etl_importer_unit_group, foreign_key: true, type: :string, index: {name: :index_etl_importer_invoicea_fees_on_etl_importer_unit_group_id}

    add_reference :etl_importer_unit_amenities, :etl_importer_unit_group, foreign_key: true, type: :string

  end
end
