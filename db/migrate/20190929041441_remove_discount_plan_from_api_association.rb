class RemoveDiscountPlanFromApiAssociation < ActiveRecord::Migration[5.2]
  def change
    remove_reference :etl_importer_api_associations, :etl_importer_discount_plan
  end
end
