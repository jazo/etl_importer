# ## Schema Information
#
# Table name: `etl_importer_unit_group_invoiceable_fees`
#
# ### Columns
#
# Name                                    | Type               | Attributes
# --------------------------------------- | ------------------ | ---------------------------
# **`id`**                                | `uuid`             | `not null, primary key`
# **`etl_importer_unit_group_id`**        | `string`           |
# **`etl_importer_invoiceable_fees_id`**  | `uuid`             |
# **`created_at`**                        | `datetime`         | `not null`
# **`updated_at`**                        | `datetime`         | `not null`
#
# ### Indexes
#
# * `index_etl_importer_un_gpo_inv_fees_on_etl_importer_inv_fees_id`:
#     * **`etl_importer_invoiceable_fees_id`**
# * `index_etl_importer_un_gpo_invo_fees_on_etl_importer_un_gpo_id`:
#     * **`etl_importer_unit_group_id`**
#

require 'rails_helper'

module EtlImporter
  RSpec.describe UnitGroupInvoiceableFee, type: :model do
    pending "add some examples to (or delete) #{__FILE__}"
  end
end
