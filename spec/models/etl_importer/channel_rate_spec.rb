# ## Schema Information
#
# Table name: `etl_importer_channel_rates`
#
# ### Columns
#
# Name                            | Type               | Attributes
# ------------------------------- | ------------------ | ---------------------------
# **`id`**                        | `uuid`             | `not null, primary key`
# **`base_rate_type`**            | `string`           |
# **`modifier_type`**             | `string`           |
# **`turned_on`**                 | `boolean`          |
# **`turned_off_on`**             | `boolean`          |
# **`rate`**                      | `decimal(, )`      |
# **`amount`**                    | `decimal(, )`      |
# **`channel_name`**              | `string`           |
# **`etl_importer_channel_id`**   | `uuid`             |
# **`etl_importer_facility_id`**  | `uuid`             |
# **`created_at`**                | `datetime`         | `not null`
# **`updated_at`**                | `datetime`         | `not null`
#
# ### Indexes
#
# * `index_etl_importer_channel_rates_on_etl_importer_channel_id`:
#     * **`etl_importer_channel_id`**
# * `index_etl_importer_channel_rates_on_etl_importer_facility_id`:
#     * **`etl_importer_facility_id`**
#

require 'rails_helper'

module EtlImporter
  RSpec.describe ChannelRate, type: :model do
    pending "add some examples to (or delete) #{__FILE__}"
  end
end
