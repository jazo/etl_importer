# ## Schema Information
#
# Table name: `etl_importer_discount_plans`
#
# ### Columns
#
# Name                                                 | Type               | Attributes
# ---------------------------------------------------- | ------------------ | ---------------------------
# **`id`**                                             | `uuid`             | `not null, primary key`
# **`name`**                                           | `string`           |
# **`description`**                                    | `text`             |
# **`public_description`**                             | `text`             |
# **`availability_text`**                              | `text`             |
# **`requirements_text`**                              | `text`             |
# **`kind`**                                           | `string`           |
# **`min_occupancy_required`**                         | `boolean`          |
# **`min_occupancy_months`**                           | `boolean`          |
# **`prepay_required`**                                | `boolean`          |
# **`prepay_months`**                                  | `integer`          |
# **`start_date`**                                     | `date`             |
# **`end_date`**                                       | `date`             |
# **`auto_apply`**                                     | `boolean`          |
# **`promotion_type`**                                 | `string`           |
# **`deleted`**                                        | `boolean`          |
# **`turned_on`**                                      | `boolean`          |
# **`available_for_all_facilities`**                   | `boolean`          |
# **`round_to_nearest`**                               | `string`           |
# **`hide_from_website`**                              | `boolean`          |
# **`move_in_only`**                                   | `boolean`          |
# **`existing_tenant_only`**                           | `boolean`          |
# **`priority`**                                       | `integer`          |
# **`role_permission`**                                | `string`           |
# **`etl_importer_always_discount_plan_discount_id`**  | `uuid`             |
# **`etl_importer_tenant_account_kind_id`**            | `uuid`             |
# **`created_at`**                                     | `datetime`         | `not null`
# **`updated_at`**                                     | `datetime`         | `not null`
#
# ### Indexes
#
# * `index_etl_importer_d_plan_on_etl_importer_always_dis_id`:
#     * **`etl_importer_always_discount_plan_discount_id`**
# * `index_etl_importer_d_plan_on_etl_importer_tenant_acco_kind_id`:
#     * **`etl_importer_tenant_account_kind_id`**
#

require 'rails_helper'

module EtlImporter
  RSpec.describe DiscountPlan, type: :model do
    pending "add some examples to (or delete) #{__FILE__}"
  end
end
