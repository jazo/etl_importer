# ## Schema Information
#
# Table name: `etl_importer_discount_plan_client_applications`
#
# ### Columns
#
# Name                                      | Type               | Attributes
# ----------------------------------------- | ------------------ | ---------------------------
# **`id`**                                  | `uuid`             | `not null, primary key`
# **`etl_importer_discount_plan_id`**       | `uuid`             |
# **`etl_importer_client_application_id`**  | `uuid`             |
# **`created_at`**                          | `datetime`         | `not null`
# **`updated_at`**                          | `datetime`         | `not null`
#
# ### Indexes
#
# * `index_etl_importer_d_plan_client_app_on_etl_importer_d_plan_id`:
#     * **`etl_importer_discount_plan_id`**
# * `index_etl_importer_d_pln_cli_apps_on_etl_importer_client_app_id`:
#     * **`etl_importer_client_application_id`**
#

require 'rails_helper'

module EtlImporter
  RSpec.describe DiscountPlanClientApplication, type: :model do
    pending "add some examples to (or delete) #{__FILE__}"
  end
end
