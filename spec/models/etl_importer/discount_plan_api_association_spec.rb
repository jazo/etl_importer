# ## Schema Information
#
# Table name: `etl_importer_discount_plan_api_associations`
#
# ### Columns
#
# Name                                   | Type               | Attributes
# -------------------------------------- | ------------------ | ---------------------------
# **`id`**                               | `uuid`             | `not null, primary key`
# **`etl_importer_discount_plan_id`**    | `uuid`             |
# **`etl_importer_api_association_id`**  | `uuid`             |
# **`created_at`**                       | `datetime`         | `not null`
# **`updated_at`**                       | `datetime`         | `not null`
#
# ### Indexes
#
# * `index_etl_importer_d_plan_api_assoc_on_etl_importer_api_asso_id`:
#     * **`etl_importer_api_association_id`**
# * `index_etl_importer_d_plan_api_assoc_on_etl_importer_d_plan_id`:
#     * **`etl_importer_discount_plan_id`**
#

require 'rails_helper'

module EtlImporter
  RSpec.describe DiscountPlanApiAssociation, type: :model do
    pending "add some examples to (or delete) #{__FILE__}"
  end
end
