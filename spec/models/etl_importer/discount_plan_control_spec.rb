# ## Schema Information
#
# Table name: `etl_importer_discount_plan_controls`
#
# ### Columns
#
# Name                                   | Type               | Attributes
# -------------------------------------- | ------------------ | ---------------------------
# **`id`**                               | `uuid`             | `not null, primary key`
# **`kind`**                             | `string`           |
# **`min_value`**                        | `decimal(, )`      |
# **`max_value`**                        | `decimal(, )`      |
# **`deleted`**                          | `boolean`          |
# **`deleted_on`**                       | `date`             |
# **`requirements_text`**                | `text`             |
# **`applicable_discount_plans_count`**  | `integer`          |
# **`etl_importer_unit_amenity_id`**     | `uuid`             |
# **`etl_importer_unit_type_id`**        | `uuid`             |
# **`created_at`**                       | `datetime`         | `not null`
# **`updated_at`**                       | `datetime`         | `not null`
#
# ### Indexes
#
# * `index_etl_importer_d_plan_cont_on_etl_importer_unit_amenity_id`:
#     * **`etl_importer_unit_amenity_id`**
# * `index_etl_importer_d_plan_controls_on_etl_importer_unit_type_id`:
#     * **`etl_importer_unit_type_id`**
#

require 'rails_helper'

module EtlImporter
  RSpec.describe DiscountPlanControl, type: :model do
    pending "add some examples to (or delete) #{__FILE__}"
  end
end
