# ## Schema Information
#
# Table name: `etl_importer_discount_plan_facilities`
#
# ### Columns
#
# Name                                 | Type               | Attributes
# ------------------------------------ | ------------------ | ---------------------------
# **`id`**                             | `uuid`             | `not null, primary key`
# **`etl_importer_discount_plan_id`**  | `uuid`             |
# **`etl_importer_facility_id`**       | `uuid`             |
# **`created_at`**                     | `datetime`         | `not null`
# **`updated_at`**                     | `datetime`         | `not null`
#
# ### Indexes
#
# * `index_etl_importer_d_plan_facilites_on_etl_importer_facility_id`:
#     * **`etl_importer_facility_id`**
# * `index_etl_importer_d_plan_facilities_on_etl_importer_d_plan_id`:
#     * **`etl_importer_discount_plan_id`**
#

require 'rails_helper'

module EtlImporter
  RSpec.describe DiscountPlanFacility, type: :model do
    pending "add some examples to (or delete) #{__FILE__}"
  end
end
