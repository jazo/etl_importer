# ## Schema Information
#
# Table name: `etl_importer_invoiceable_fees`
#
# ### Columns
#
# Name                        | Type               | Attributes
# --------------------------- | ------------------ | ---------------------------
# **`id`**                    | `uuid`             | `not null, primary key`
# **`product_code`**          | `string`           |
# **`kind`**                  | `string`           |
# **`description`**           | `string`           |
# **`required_at_move_in`**   | `boolean`          |
# **`required_at_transfer`**  | `boolean`          |
# **`amount`**                | `decimal(, )`      |
# **`short_description`**     | `text`             |
# **`show_in_sales_center`**  | `boolean`          |
# **`tax_total`**             | `decimal(, )`      |
# **`total`**                 | `decimal(, )`      |
# **`created_at`**            | `datetime`         | `not null`
# **`updated_at`**            | `datetime`         | `not null`
#

require 'rails_helper'

module EtlImporter
  RSpec.describe InvoiceableFee, type: :model do
    pending "add some examples to (or delete) #{__FILE__}"
  end
end
