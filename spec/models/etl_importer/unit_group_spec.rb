# ## Schema Information
#
# Table name: `etl_importer_unit_groups`
#
# ### Columns
#
# Name                                  | Type               | Attributes
# ------------------------------------- | ------------------ | ---------------------------
# **`price`**                           | `decimal(, )`      |
# **`group_key`**                       | `string`           |
# **`name`**                            | `string`           |
# **`available_units_count`**           | `integer`          |
# **`total_units_count`**               | `integer`          |
# **`total_non_excluded_units_count`**  | `integer`          |
# **`size`**                            | `string`           |
# **`standard_rate`**                   | `integer`          |
# **`floor`**                           | `integer`          |
# **`reduced_price`**                   | `decimal(, )`      |
# **`occupancy_percent`**               | `integer`          |
# **`area`**                            | `integer`          |
# **`length`**                          | `integer`          |
# **`width`**                           | `integer`          |
# **`height`**                          | `integer`          |
# **`due_at_move_in`**                  | `integer`          |
# **`due_at_move_in_without_fees`**     | `integer`          |
# **`due_monthly`**                     | `integer`          |
# **`attribute_description`**           | `text`             |
# **`description`**                     | `text`             |
# **`average_rent`**                    | `integer`          |
# **`active_rate_type`**                | `string`           |
# **`etl_importer_channel_rate_id`**    | `uuid`             |
# **`etl_importer_unit_type_id`**       | `uuid`             |
# **`created_at`**                      | `datetime`         | `not null`
# **`updated_at`**                      | `datetime`         | `not null`
# **`id`**                              | `string`           | `not null, primary key`
#
# ### Indexes
#
# * `index_etl_importer_unit_groups_on_etl_importer_channel_rate_id`:
#     * **`etl_importer_channel_rate_id`**
# * `index_etl_importer_unit_groups_on_etl_importer_unit_type_id`:
#     * **`etl_importer_unit_type_id`**
#

require 'rails_helper'

module EtlImporter
  RSpec.describe UnitGroup, type: :model do
    pending "add some examples to (or delete) #{__FILE__}"
  end
end
