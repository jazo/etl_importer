# ## Schema Information
#
# Table name: `etl_importer_facilities`
#
# ### Columns
#
# Name              | Type               | Attributes
# ----------------- | ------------------ | ---------------------------
# **`id`**          | `uuid`             | `not null, primary key`
# **`created_at`**  | `datetime`         | `not null`
# **`updated_at`**  | `datetime`         | `not null`
#

require 'rails_helper'

module EtlImporter
  RSpec.describe Facility, type: :model do
    pending "add some examples to (or delete) #{__FILE__}"
  end
end
