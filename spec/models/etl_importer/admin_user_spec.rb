# ## Schema Information
#
# Table name: `etl_importer_admin_users`
#
# ### Columns
#
# Name                          | Type               | Attributes
# ----------------------------- | ------------------ | ---------------------------
# **`id`**                      | `integer`          | `not null, primary key`
# **`email`**                   | `string`           | `default(""), not null`
# **`encrypted_password`**      | `string`           | `default(""), not null`
# **`reset_password_token`**    | `string`           |
# **`reset_password_sent_at`**  | `datetime`         |
# **`remember_created_at`**     | `datetime`         |
#
# ### Indexes
#
# * `index_etl_importer_admin_users_on_email` (_unique_):
#     * **`email`**
# * `index_etl_importer_admin_users_on_reset_password_token` (_unique_):
#     * **`reset_password_token`**
#

require 'rails_helper'

module EtlImporter
  RSpec.describe AdminUser, type: :model do
    pending "add some examples to (or delete) #{__FILE__}"
  end
end
