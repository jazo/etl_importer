# ## Schema Information
#
# Table name: `etl_importer_unit_group_unit_amenities`
#
# ### Columns
#
# Name                                  | Type               | Attributes
# ------------------------------------- | ------------------ | ---------------------------
# **`id`**                              | `uuid`             | `not null, primary key`
# **`etl_importer_unit_amenities_id`**  | `uuid`             |
# **`created_at`**                      | `datetime`         | `not null`
# **`updated_at`**                      | `datetime`         | `not null`
# **`etl_importer_unit_group_id`**      | `string`           |
#
# ### Indexes
#
# * `index_etl_importer_uni_group_un_amen_on_etl_importer_un_gop_id`:
#     * **`etl_importer_unit_group_id`**
# * `index_etl_importer_unit_gop_un_amen_on_etl_importer_un_amen_id`:
#     * **`etl_importer_unit_amenities_id`**
#

require 'rails_helper'

module EtlImporter
  RSpec.describe UnitGroupUnitAmenity, type: :model do
    pending "add some examples to (or delete) #{__FILE__}"
  end
end
