FactoryBot.define do
  factory :etl_importer_api_association, class: 'ApiAssociation' do
    discount_plan { nil }
  end
end
