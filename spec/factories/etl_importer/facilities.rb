FactoryBot.define do
  factory :etl_importer_facility, class: 'Facility' do
    discount_plan { nil }
  end
end
