FactoryBot.define do
  factory :etl_importer_unit, class: 'Unit' do
    price { "9.99" }
    name { "MyString" }
    description { "MyText" }
    width { 1 }
    length { 1 }
    height { 1 }
    door_height { 1 }
    door_width { 1 }
    door_type { "MyString" }
    access_type { "MyString" }
    floor { "MyString" }
    size { "MyString" }
    area { 1 }
    standard_rate { 1 }
    managed_rate { "MyString" }
    available_for_move_in { false }
    rentable { false }
    status { "MyString" }
    payment_status { "MyString" }
    current_ledger_id { "MyString" }
    current_tenant_id { "MyString" }
    combination_lock_number { "MyString" }
    attribute_description { "MyString" }
    deleted { false }
    damaged { false }
    complimentary { false }
    channel_rate { nil }
  end
end
