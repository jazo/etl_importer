FactoryBot.define do
  factory :etl_importer_discount_plan, class: 'DiscountPlan' do
    name { "MyString" }
    description { "MyText" }
    public_description { "MyText" }
    availability_text { "MyText" }
    requirements_text { "MyText" }
    kind { "MyString" }
    min_occupancy_required { false }
    min_occupancy_months { false }
    prepay_required { false }
    prepay_months { 1 }
    start_date { "2019-09-28" }
    end_date { "2019-09-28" }
    auto_apply { false }
    promotion_type { "MyString" }
    deleted { false }
    turned_on { false }
    available_for_all_facilities { false }
    round_to_nearest { "MyString" }
    hide_from_website { false }
    move_in_only { false }
    existing_tenant_only { false }
    priority { 1 }
    role_permission { "MyString" }
    always_discount_plan_discount { nil }
    tenant_account_kind { nil }
    unit_group { nil }
  end
end
