FactoryBot.define do
  factory :etl_importer_unit_type, class: 'UnitType' do
    name { "MyString" }
    deleted { false }
    internal_account_code { "MyString" }
    code_and_description { "MyText" }
  end
end
