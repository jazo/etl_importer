FactoryBot.define do
  factory :etl_importer_discount_plan_client_application, class: 'DiscountPlanClientApplication' do
    etl_importer_discount_plan { nil }
    etl_importer_client_application { nil }
  end
end
