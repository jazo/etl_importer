FactoryBot.define do
  factory :etl_importer_discount_plan_discount, class: 'DiscountPlanDiscount' do
    discount_plan { nil }
    discount_type { "MyString" }
    month_number { 1 }
    amount { "9.99" }
    minimum_amount { "9.99" }
    maximum_amount { "9.99" }
  end
end
