FactoryBot.define do
  factory :etl_importer_unit_group, class: 'UnitGroup' do
    price { "9.99" }
    group_key { "MyString" }
    name { "MyString" }
    available_units_count { 1 }
    total_units_count { 1 }
    total_non_excluded_units_count { 1 }
    size { "MyString" }
    standard_rate { 1 }
    floor { 1 }
    reduced_price { "9.99" }
    occupancy_percent { 1 }
    area { 1 }
    length { 1 }
    width { 1 }
    height { 1 }
    due_at_move_in { 1 }
    due_at_move_in_without_fees { 1 }
    due_monthly { 1 }
    attribute_description { "MyText" }
    description { "MyText" }
    average_rent { 1 }
    active_rate_type { "MyString" }
    channel_rate { nil }
    unit_type { nil }
  end
end
