FactoryBot.define do
  factory :etl_importer_unit_group_invoiceable_fee, class: 'UnitGroupInvoiceableFee' do
    unit_group { nil }
    invoiceable_fees { nil }
  end
end
