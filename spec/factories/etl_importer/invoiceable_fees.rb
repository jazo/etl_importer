FactoryBot.define do
  factory :etl_importer_invoiceable_fee, class: 'InvoiceableFee' do
    product_code { "MyString" }
    kind { "MyString" }
    description { "MyString" }
    required_at_move_in { false }
    required_at_transfer { false }
    amount { "9.99" }
    short_description { "MyText" }
    show_in_sales_center { false }
    tax_total { "9.99" }
    total { "9.99" }
    unit_group { nil }
  end
end
