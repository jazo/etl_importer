FactoryBot.define do
  factory :etl_importer_discount_plan_api_association, class: 'DiscountPlanApiAssociation' do
    etl_importer_discount_plan { nil }
    etl_importer_api_association { nil }
  end
end
