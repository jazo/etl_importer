FactoryBot.define do
  factory :etl_importer_discount_plan_control, class: 'DiscountPlanControl' do
    kind { "MyString" }
    min_value { "9.99" }
    max_value { "9.99" }
    deleted { false }
    deleted_on { "2019-09-28" }
    requirements_text { "MyText" }
    applicable_discount_plans_count { 1 }
    unit_amenity { nil }
    unit_type { nil }
  end
end
