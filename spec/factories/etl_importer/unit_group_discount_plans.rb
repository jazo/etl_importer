FactoryBot.define do
  factory :etl_importer_unit_group_discount_plan, class: 'UnitGroupDiscountPlan' do
    unit_group { nil }
    discount_plan { nil }
  end
end
