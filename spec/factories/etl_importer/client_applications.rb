FactoryBot.define do
  factory :etl_importer_client_application, class: 'ClientApplication' do
    discount_plan { nil }
    name { "MyString" }
    channel_rates_on { false }
    internal { false }
  end
end
