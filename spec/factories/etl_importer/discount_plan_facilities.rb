FactoryBot.define do
  factory :etl_importer_discount_plan_facility, class: 'DiscountPlanFacility' do
    discount_plan { nil }
    facility { nil }
  end
end
