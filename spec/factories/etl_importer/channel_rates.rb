FactoryBot.define do
  factory :etl_importer_channel_rate, class: 'ChannelRate' do
    base_rate_type { "MyString" }
    modifier_type { "MyString" }
    turned_on { false }
    turned_off_on { false }
    rate { "9.99" }
    amount { "9.99" }
    channel_name { "MyString" }
    channel { nil }
    facility { nil }
  end
end
