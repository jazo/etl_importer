FactoryBot.define do
  factory :etl_importer_discount_plan_discount_plan_control, class: 'DiscountPlanDiscountPlanControl' do
    discount_plan { nil }
    discount_plan_control { nil }
  end
end
