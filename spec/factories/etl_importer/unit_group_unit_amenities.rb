FactoryBot.define do
  factory :etl_importer_unit_group_unit_amenity, class: 'UnitGroupUnitAmenity' do
    etl_importer_unit_group { nil }
    etl_importer_unit_amenities { nil }
  end
end
