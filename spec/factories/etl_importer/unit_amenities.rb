FactoryBot.define do
  factory :etl_importer_unit_amenity, class: 'UnitAmenity' do
    name { "MyString" }
    short_code { "MyString" }
    show_in_sales_center_filter_dropdown { false }
    unit_group { nil }
  end
end
