require 'rails_helper'

module EtlImporter
  RSpec.describe UnitGroupImporter do
    describe '#call' do
      context 'successful parse' do
        let(:file) { file_fixture('unit_groups.json').read }
        let(:unit_groups_data) {
          json = JSON.parse(file, symbolize_names: true)
          json[:unit_groups]
        }
        let(:first_unit_group) { unit_groups_data[0] }
        subject(:importer) { UnitGroupImporter.call(file) }

        it {
          expect(importer).to eq([first_unit_group[:id]])
        }

        describe 'change model counts' do
          let(:unit_group_size) { unit_groups_data.size }
          let(:invoiceable_fee_size) { first_unit_group[:invoiceable_fees].size }
          let(:channel_rate_size) { first_unit_group[:channel_rate_ids].size }
          let(:unit_amenity_size) { first_unit_group[:unit_amenities].size }
          let(:unit_type_size) { 1 }
          let(:discount_plan_size) { first_unit_group[:discount_plans].size }
          let(:discount_plans_data) { first_unit_group[:discount_plans] }
          let(:discount_plan_discount_size) {
            discount_plans_data.map {|d| d[:discount_plan_discounts].size }.inject(0, :+)
          }
          let(:discount_plan_control_size) {
            discount_plans_data.map {|d| d[:discount_plan_controls].size }.inject(0, :+)
          }
          let(:api_association_size) {
            discount_plans_data.map {|d| d[:api_association_ids] }.flatten.uniq.size
          }
          let(:client_application_size) {
            discount_plans_data.map {|d| d[:client_applications] }.flatten.uniq.size
          }
          let(:always_discount_size) {
            adpd = discount_plans_data.map do |d|
              d[:always_discount_plan_discount_id].present?
            end
            adpd.select {|d| d == true }.size
          }
          let(:facility_size) {
            discount_plans_data.map {|d| d[:facility_ids] }.flatten.uniq.size
          }
          it { expect { importer }.to change(UnitGroup, :count).by(unit_group_size) }
          it {
            expect { importer }.to change(InvoiceableFee, :count).by(invoiceable_fee_size)
          }
          it { expect { importer }.to change(ChannelRate, :count).by(channel_rate_size) }
          it { expect { importer }.to change(UnitAmenity, :count).by(unit_amenity_size) }
          it { expect { importer }.to change(UnitType, :count).by(unit_type_size) }
          it {
            expect { importer }.to change(DiscountPlan, :count).by(discount_plan_size)
          }
          it {
            expect { importer }.to change(DiscountPlanDiscount, :count).by(
              discount_plan_discount_size
            )
          }
          it {
            expect { importer }.to change(DiscountPlanControl, :count).by(
              discount_plan_control_size
            )
          }
          it {
            expect { importer }.to change(ApiAssociation, :count).by(api_association_size)
          }
          it {
            expect { importer }.to change(ClientApplication, :count).by(
              client_application_size
            )
          }
          it {
            expect { importer }.to change(AlwaysDiscountPlanDiscount, :count).by(
              always_discount_size
            )
          }
          it { expect { importer }.to change(Facility, :count).by(facility_size) }
        end

        context 'should create associated models' do
          before :each do
            UnitGroupImporter.call(file)
          end

          describe 'UnitGroup' do
            let(:model) { UnitGroup.first }
            it { expect(model).to be_valid }
            it {
              expect(model.id).to eq(first_unit_group[:id])
            }
          end

          describe 'InvoiceableFee' do
            let(:model) { InvoiceableFee.first }
            it { expect(model).to be_valid }
            it {
              expect(model.id).to eq(first_unit_group[:invoiceable_fees][0][:id])
            }
          end

          describe 'ChannelRate' do
            let(:model) { ChannelRate.first }
            it { expect(model).to be_valid }
            it {
              expect(model.id).to eq(first_unit_group[:channel_rate][:id])
            }
          end

          describe 'UnitAmenity' do
            let(:unit_amenities) {
              first_unit_group[:unit_amenities].map {|e| e[:id] }
            }
            let(:model) { UnitAmenity.first }
            it { expect(model).to be_valid }
            it { expect(unit_amenities).to include(model.id) }
          end

          describe 'UnitType' do
            let(:model) { UnitType.first }
            it { expect(model).to be_valid }
            it {
              expect(model.id).to eq(first_unit_group[:unit_type][:id])
            }
          end
        end

        context 'should have all relations' do
          before :each do
            UnitGroupImporter.call(file)
          end

          let(:unit_group) { UnitGroup.first }
          let(:invoiceable_fee_ids) {
            first_unit_group[:invoiceable_fees].map {|e| e[:id] }
          }
          let(:channel_rate_id) { first_unit_group[:channel_rate][:id] }
          let(:unit_amenities_ids) { first_unit_group[:unit_amenities].map {|e| e[:id] } }
          let(:unit_type_id) { first_unit_group[:unit_type][:id] }
          let(:discount_plan_ids) { first_unit_group[:discount_plans].map {|e| e[:id] } }

          it 'invoiceable fees' do
            expect(unit_group.invoiceable_fee_ids.sort).to eq(invoiceable_fee_ids.sort)
          end

          it 'channel rate' do
            expect(unit_group.channel_rate.id).to eq(channel_rate_id)
          end

          it 'unit amenities' do
            expect(unit_group.unit_amenity_ids.sort).to eq(unit_amenities_ids.sort)
          end

          it 'unit type' do
            expect(unit_group.unit_type.id).to eq(unit_type_id)
          end

          it 'discount plans' do
            expect(unit_group.discount_plan_ids.sort).to eq(discount_plan_ids.sort)
          end
        end

        context 'shoud create discount plan' do
          before :each do
            UnitGroupImporter.call(file)
          end

          let(:unit_group) { UnitGroup.first }
          let(:first_discount_plan_data) { first_unit_group[:discount_plans].first }
          let(:discount_plan) {
            unit_group.discount_plans.find(first_discount_plan_data[:id])
          }
          let(:discount_plan_ids) {
            first_unit_group[:discount_plans].map {|e| e[:id] }
          }
          let(:discount_plan_discount_ids) {
            first_discount_plan_data[:discount_plan_discounts].map {|e| e[:id] }
          }
          let(:discount_plan_control_ids) {
            first_discount_plan_data[:discount_plan_controls].map {|e| e[:id] }
          }
          let(:api_association_ids) { first_discount_plan_data[:api_association_ids] }
          let(:client_application_ids) {
            first_discount_plan_data[:client_applications].map {|e| e[:id] }
          }
          let(:always_discount_plan_discount_id) {
            first_discount_plan_data[:always_discount_plan_discount_id]
          }
          let(:tenant_account_kind_id) {
            first_discount_plan_data[:tenant_account_kind_id]
          }
          let(:facility_ids) { first_discount_plan_data[:facility_ids].uniq }

          describe 'DiscountPlan' do
            let(:model) { DiscountPlan.first }
            it { expect(discount_plan).to be_valid }
            it { expect(discount_plan_ids).to include(model.id) }
          end

          describe 'with relations' do
            describe 'TenantAccountKind' do
              let(:model) { TenantAccountKind.first }
              it { expect(model).to be_valid }
              it {
                expect(discount_plan.etl_importer_tenant_account_kind_id)
                  .to eq(tenant_account_kind_id)
              }
            end
            describe 'DiscountPlanDiscount' do
              let(:model) { DiscountPlanDiscount.first }
              it { expect(model).to be_valid }
              it {
                expect(discount_plan.discount_plan_discount_ids)
                  .to eq(discount_plan_discount_ids)
              }
            end

            describe 'DiscountPlanControl' do
              let(:model) { DiscountPlanControl.first }
              it { expect(model).to be_valid }
              it {
                expect(discount_plan.discount_plan_control_ids)
                  .to eq(discount_plan_control_ids)
              }
            end

            describe 'ApiAssociation' do
              let(:model) { ApiAssociation.first }
              it { expect(model).to be_valid }
              it {
                expect(discount_plan.api_association_ids).to eq(api_association_ids)
              }
            end

            describe 'ClientApplication' do
              let(:model) { ClientApplication.first }
              it { expect(model).to be_valid }
              it {
                expect(discount_plan.client_application_ids).to eq(client_application_ids)
              }
            end

            describe 'AlwaysDiscountPlanDiscount' do
              let(:model) { AlwaysDiscountPlanDiscount.first }
              it { expect(model).to be_valid }
              it {
                expect(discount_plan.etl_importer_always_discount_plan_discount_id)
                  .to eq(always_discount_plan_discount_id)
              }
            end

            describe 'Facility' do
              let(:model) { Facility.first }
              it { expect(model).to be_valid }
              it {
                expect(discount_plan.facility_ids)
                  .to eq(facility_ids)
              }
            end
          end
        end
      end

      context 'failed parse' do
        let(:file) { file_fixture('fail_unit_groups.json').read }
      end
    end
  end
end
