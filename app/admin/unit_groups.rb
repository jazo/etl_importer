ActiveAdmin.register EtlImporter::UnitGroup, as: 'Unit Group' do
  menu label: 'Unit Group'

  filter :price
  filter :name
  filter :description
  filter :available_units_count
  filter :total_units_count
  filter :total_non_excluded_units_count
  filter :size
  filter :standard_rate
  filter :floor
  filter :reduced_price
  filter :occupancy_percent
  filter :area
  filter :length
  filter :width
  filter :height
  filter :created_at
  filter :updated_at

  index do
    selectable_column
    id_column
    column :price
    column :name
    column :description
    column :available_units_count
    column :total_units_count
    column :total_non_excluded_units_count
    column :size
    column :standard_rate
    column :floor
    column :reduced_price
    column :occupancy_percent
    column :area
    column :length
    column :width
    column :height
    column :created_at
    column :updated_at
    actions
  end

  show do
    tabs do
      tab 'General' do
        attributes_table do
          row :price
          row :group_key
          row :name
          row :available_units_count
          row :total_units_count
          row :total_non_excluded_units_count
          row :size
          row :standard_rate
          row :floor
          row :reduced_price
          row :occupancy_percent
          row :area
          row :length
          row :width
          row :height
          row :due_at_move_in
          row :due_at_move_in_without_fees
          row :due_monthly
          row :attribute_description
          row :description
          row :average_rent
          row :active_rate_type
        end
      end
      tab 'Invoiceable Fees' do
        panel 'Invoiceable Fees' do
          table_for unit_group.invoiceable_fees do
            column :product_code
            column :kind
            column :description
            column :required_at_move_in
            column :required_at_transfer
            column :amount
            column :short_description
            column :show_in_sales_center
            column :tax_total
            column :total
          end
        end
      end
      tab 'Channel Rate' do
        panel 'Channel Rate' do
          attributes_table_for unit_group.channel_rate do
            row :base_rate_type
            row :modifier_type
            row :turned_on
            row :turned_off_on
            row :rate
            row :amount
            row :channel_name
            row :channel
            row :facility
          end
        end
      end
      tab 'Unit Amenities' do
        panel 'Unit Amenities' do
          table_for unit_group.unit_amenities do
            column :name
            column :short_code
            column :show_in_sales_center_filter_dropdown
          end
        end
      end
      tab 'Unit Type' do
        panel 'Unit Type' do
          attributes_table_for unit_group.channel_rate do
            row :name
            row :deleted
            row :internal_account_code
            row :code_and_description
          end
        end
      end
      tab 'Discount Plans' do
        panel 'Discount Plans' do
          table_for unit_group.discount_plans do
            column :name do |discount_plan|
              link_to discount_plan.name, admin_discount_plan_path(discount_plan)
            end
            column :description
            column :public_description
            column :availability_text
            column :requirements_text
            column :kind
            column :min_occupancy_required
            column :min_occupancy_months
            column :prepay_required
            column :prepay_months
            column :start_date
            column :end_date
            column :auto_apply
            column :promotion_type
            column :deleted
            column :turned_on
            column :available_for_all_facilities
            column :round_to_nearest
            column :hide_from_website
            column :move_in_only
            column :tenant_account_kind
            column :existing_tenant_only
            column :priority
          end
        end
      end
    end
  end
end
