ActiveAdmin.register EtlImporter::DiscountPlan, as: 'Discount Plan' do
  index do
    selectable_column
    id_column
    column :description
    column :public_description
    column :availability_text
    column :requirements_text
    column :kind
    column :min_occupancy_required
    column :min_occupancy_months
    column :prepay_required
    column :prepay_months
    column :start_date
    column :end_date
    column :auto_apply
    column :promotion_type
    column :deleted
    column :turned_on
    column :available_for_all_facilities
    column :round_to_nearest
    column :hide_from_website
    column :move_in_only
    column :tenant_account_kind
    column :existing_tenant_only
    column :priority
    actions
  end

  show do
    tabs do
      tab 'General' do
        attributes_table do
          row :name
          row :description
          row :public_description
          row :availability_text
          row :requirements_text
          row :kind
          row :min_occupancy_required
          row :min_occupancy_months
          row :prepay_required
          row :prepay_months
          row :start_date
          row :end_date
          row :auto_apply
          row :promotion_type
          row :deleted
          row :turned_on
          row :available_for_all_facilities
          row :round_to_nearest
          row :hide_from_website
          row :move_in_only
          row :tenant_account_kind_id
          row :existing_tenant_only
          row :priority
          row :role_permission
        end
      end
      tab 'Discounts' do
        panel 'Discounts' do
          table_for discount_plan.discount_plan_discounts do
            column :id
            column :discount_type
            column :month_number
            column :amount
            column :minimum_amount
            column :maximum_amount
          end
        end
      end
      tab 'Controls' do
        panel 'Controls' do
          table_for discount_plan.discount_plan_controls do
            column :id
            column :kind
            column :min_value
            column :max_value
            column :deleted
            column :deleted_on
            column :requirements_text
            column :applicable_discount_plans_count
          end
        end
      end
      tab 'Api Association' do
        panel 'Api Association' do
          table_for discount_plan.api_associations do
            column :id
          end
        end
      end
      tab 'Client Applications' do
        panel 'Client Applications' do
          table_for discount_plan.client_applications do
            column :id
            column :name
            column :channel_rates_on
            column :internal
          end
        end
      end
      tab 'Always Discount Plan Discount' do
        panel 'Always Discount Plan Discount' do
          attributes_table_for discount_plan.always_discount_plan_discount do
            row :id
          end
        end
      end
      tab 'Facilities' do
        panel 'Facilities' do
          table_for discount_plan.facilities do
            column :id
          end
        end
      end
    end
  end
end
