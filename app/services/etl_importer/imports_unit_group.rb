module EtlImporter
  class ImportsUnitGroup
    extend LightService::Organizer

    def self.call(unit_group_data)
      with(unit_group_data: unit_group_data).reduce(
        ExtractUnitType,
        ExtractChannelRate,
        ExtractUnitGroup,
        ExtractInvoiceableFees,
        ExtractUnitAmenities,
        ExtractDiscountPlans
      )
    end
  end
end
