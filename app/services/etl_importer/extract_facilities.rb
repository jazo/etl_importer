module EtlImporter
  class ExtractFacilities
    extend LightService::Action
    expects :discount_plan_data, :discount_plan

    executed do |ctx|
      if ctx.discount_plan_data[:facility_ids].present?
        ctx.discount_plan_data[:facility_ids].each do |facility_id|
          facility = EtlImporter::Facility.find_or_create_by(
            id: facility_id
          )
          EtlImporter::DiscountPlanFacility.find_or_create_by(
            discount_plan: ctx.discount_plan,
            facility:      facility
          )
        end
      end
    end
  end
end
