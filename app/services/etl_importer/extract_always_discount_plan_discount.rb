module EtlImporter
  class ExtractAlwaysDiscountPlanDiscount
    extend LightService::Action
    expects :discount_plan_data
    promises :always_discount

    executed do |ctx|
      ctx.always_discount = nil

      if ctx.discount_plan_data[:always_discount_plan_discount_id].present?
        ctx.always_discount = EtlImporter::AlwaysDiscountPlanDiscount.find_or_create_by(
          id: ctx.discount_plan_data[:always_discount_plan_discount_id]
        )
      end
    end
  end
end
