module EtlImporter
  class ExtractDiscountPlan
    extend LightService::Action
    expects :discount_plan_data, :tenant_account_kind, :always_discount, :unit_group
    promises :discount_plan

    DISCOUNT_PLAN_EXCLUDE = %i[
      discount_plan_discounts
      discount_plan_controls
      api_association_ids
      client_applications
      always_discount_plan_discount_id
      facility_ids
      tenant_account_kind_id
    ].freeze

    executed do |ctx|
      discount_plan = EtlImporter::DiscountPlan.find_or_create_by(
        ctx.discount_plan_data.except(*DISCOUNT_PLAN_EXCLUDE)
      )
      discount_plan.update(
        tenant_account_kind:           ctx.tenant_account_kind,
        always_discount_plan_discount: ctx.always_discount
      )
      EtlImporter::UnitGroupDiscountPlan.find_or_create_by(
        unit_group:    ctx.unit_group,
        discount_plan: discount_plan
      )
      ctx.discount_plan = discount_plan
    end
  end
end
