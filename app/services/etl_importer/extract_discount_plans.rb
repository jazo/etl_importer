module EtlImporter
  class ExtractDiscountPlans
    extend LightService::Action
    expects :unit_group_data, :unit_group
    promises :discount_plans

    executed do |context|
      discount_plans_data = context.unit_group_data[:discount_plans].to_a
      context.discount_plans = discount_plans_data.map do |discount_plan_data|
        EtlImporter::ImportsDiscountPlan.call(
          discount_plan_data,
          context.unit_group
        )
      end
    end
  end
end
