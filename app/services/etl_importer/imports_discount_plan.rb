module EtlImporter
  class ImportsDiscountPlan
    extend LightService::Organizer

    def self.call(discount_plan_data, unit_group)
      with(
        discount_plan_data: discount_plan_data,
        unit_group:         unit_group
      ).reduce(
        ExtractTenantAccountKind,
        ExtractAlwaysDiscountPlanDiscount,
        ExtractDiscountPlan,
        ExtractFacilities,
        ExtractDiscountPlanDiscounts,
        ExtractDiscountPlanControls,
        ExtractApiAssociation,
        ExtractClientApplications
      )
    end
  end
end
