module EtlImporter
  class ExtractDiscountPlanControls
    extend LightService::Action
    expects :discount_plan_data, :unit_group, :discount_plan

    DISCOUNT_PLAN_CONTROL_EXCLUDE = %i[
      unit_amenity_ids
      discount_plan_ids
      unit_type_id
    ].freeze

    executed do |ctx|
      ctx.discount_plan_data[:discount_plan_controls].to_a.each do |discount_data|
        discount_plan_control = EtlImporter::DiscountPlanControl.find_or_create_by(
          discount_data.except(*DISCOUNT_PLAN_CONTROL_EXCLUDE)
        )
        if discount_data[:unit_type_id].present?
          unit_type = EtlImporter::UnitType.find_or_create_by(
            id: discount_data[:unit_type_id]
          )
          discount_plan_control.update(unit_type: unit_type)
        end
        EtlImporter::DiscountPlanDiscountPlanControl.find_or_create_by(
          discount_plan:         ctx.discount_plan,
          discount_plan_control: discount_plan_control
        )

        discount_data[:unit_amenity_ids].to_a.each do |unit_amenity_id|
          unit_amenity = EtlImporter::UnitAmenity.find_or_create_by(
            id: unit_amenity_id
          )
          unit_amenity.update(
            discount_plan_control: discount_plan_control
          )
          EtlImporter::UnitGroupUnitAmenity.find_or_create_by(
            unit_group:   ctx.unit_group,
            unit_amenity: unit_amenity
          )
        end
      end
    end
  end
end
