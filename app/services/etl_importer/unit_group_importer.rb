module EtlImporter
  class UnitGroupImporter
    def self.call(file=nil)
      file ||= File.read(Rails.root.join('data', 'json', 'unit_groups.json'))
      unit_groups = JSON.parse(file, symbolize_names: true)
      unit_groups[:unit_groups].map do |unit_group_data|
        begin
          result = EtlImporter::ImportsUnitGroup.call(unit_group_data)
          result[:unit_group_id]
        rescue StandardError => e
          puts e.message
          puts e.backtrace.inspect
        end
      end
    end
  end
end
