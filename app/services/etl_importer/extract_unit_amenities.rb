module EtlImporter
  class ExtractUnitAmenities
    extend LightService::Action
    expects :unit_group_data, :unit_group

    executed do |ctx|
      ctx.unit_group_data[:unit_amenities].to_a.each do |unit_amenity_data|
        unit_amenity = EtlImporter::UnitAmenity.find_or_create_by(unit_amenity_data)
        EtlImporter::UnitGroupUnitAmenity.find_or_create_by(
          unit_group:   ctx.unit_group,
          unit_amenity: unit_amenity
        )
      end
    end
  end
end
