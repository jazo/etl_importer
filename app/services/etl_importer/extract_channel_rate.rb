module EtlImporter
  class ExtractChannelRate
    extend LightService::Action
    expects :unit_group_data
    promises :channel_rate

    CHANNEL_RATE_EXCLUDE = %i[
      channel_id
      facility_id
    ].freeze

    executed do |context|
      unit_group_data = context.unit_group_data
      context.channel_rate = nil
      if unit_group_data[:channel_rate].present?
        data = unit_group_data[:channel_rate]
        channel = EtlImporter::Channel.find_or_create_by(
          id: data[:channel_id]
        )
        facility = EtlImporter::Facility.find_or_create_by(
          id: data[:facility_id]
        )
        context.channel_rate = EtlImporter::ChannelRate.find_or_create_by(
          data.except(*CHANNEL_RATE_EXCLUDE)
              .merge(
                channel:  channel,
                facility: facility
              )
        )
      end
    end
  end
end
