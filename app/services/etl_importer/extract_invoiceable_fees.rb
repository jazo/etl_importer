module EtlImporter
  class ExtractInvoiceableFees
    extend LightService::Action
    expects :unit_group, :unit_group_data

    executed do |context|
      context.unit_group_data[:invoiceable_fees].to_a.each do |invoiceable_fee_data|
        invoiceable_fee = EtlImporter::InvoiceableFee.find_or_create_by(
          invoiceable_fee_data
        )
        EtlImporter::UnitGroupInvoiceableFee.find_or_create_by(
          unit_group:      context.unit_group,
          invoiceable_fee: invoiceable_fee
        )
      end
    end
  end
end
