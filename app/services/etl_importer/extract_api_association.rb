module EtlImporter
  class ExtractApiAssociation
    extend LightService::Action
    expects :discount_plan_data, :discount_plan

    executed do |ctx|
      ctx.discount_plan_data[:api_association_ids].to_a.each do |api_association_id|
        api_association = EtlImporter::ApiAssociation.find_or_create_by(
          id: api_association_id
        )
        DiscountPlanApiAssociation.find_or_create_by(
          api_association: api_association,
          discount_plan:   ctx.discount_plan
        )
      end
    end
  end
end
