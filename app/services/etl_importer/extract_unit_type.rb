module EtlImporter
  class ExtractUnitType
    extend LightService::Action
    expects :unit_group_data
    promises :unit_type

    executed do |ctx|
      ctx.unit_type = nil
      if ctx.unit_group_data[:unit_type].present?
        ctx.unit_type = EtlImporter::UnitType.find_or_create_by(
          ctx.unit_group_data[:unit_type]
        )
      end
    end
  end
end
