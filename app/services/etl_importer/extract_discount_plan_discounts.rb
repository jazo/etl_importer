module EtlImporter
  class ExtractDiscountPlanDiscounts
    extend LightService::Action
    expects :discount_plan_data, :discount_plan

    executed do |ctx|
      ctx.discount_plan_data[:discount_plan_discounts].to_a.each do |data|
        EtlImporter::DiscountPlanDiscount.find_or_create_by(
          data.merge(discount_plan: ctx.discount_plan)
        )
      end
    end
  end
end
