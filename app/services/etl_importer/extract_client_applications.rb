module EtlImporter
  class ExtractClientApplications
    extend LightService::Action
    expects :discount_plan_data, :discount_plan

    executed do |ctx|
      ctx.discount_plan_data[:client_applications].to_a.each do |client_application_data|
        client_application = EtlImporter::ClientApplication.find_or_create_by(
          client_application_data
        )
        EtlImporter::DiscountPlanClientApplication.find_or_create_by(
          client_application: client_application,
          discount_plan:      ctx.discount_plan
        )
      end
    end
  end
end
