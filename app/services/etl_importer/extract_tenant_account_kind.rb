module EtlImporter
  class ExtractTenantAccountKind
    extend LightService::Action
    expects :discount_plan_data
    promises :tenant_account_kind

    executed do |ctx|
      ctx.tenant_account_kind = nil

      if ctx.discount_plan_data[:tenant_account_kind_id].present?
        ctx.tenant_account_kind = EtlImporter::TenantAccountKind.find_or_create_by(
          id: ctx.discount_plan_data[:tenant_account_kind_id]
        )
      end
    end
  end
end
