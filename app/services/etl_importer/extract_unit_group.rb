module EtlImporter
  class ExtractUnitGroup
    extend LightService::Action
    expects :unit_group_data, :channel_rate, :unit_type
    promises :unit_group, :unit_group_id

    UNIT_GROUP_EXCLUDE = %i[
      invoiceable_fees
      channel_rate
      unit_amenities
      unit_type
      discount_plans
      scheduled_move_out_ids
      channel_rate_ids
    ].freeze

    executed do |ctx|
      begin
        data = ctx.unit_group_data
        ctx.unit_group = EtlImporter::UnitGroup.find_or_create_by(
          data.except(*UNIT_GROUP_EXCLUDE)
              .merge(
                unit_type:    ctx.unit_type,
                channel_rate: ctx.channel_rate
              )
        )
        ctx.unit_group_id = data[:id]
      rescue StandardError => e
        ctx.fail!(e.message)
      end
    end
  end
end
