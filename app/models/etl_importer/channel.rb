# ## Schema Information
#
# Table name: `etl_importer_channels`
#
# ### Columns
#
# Name              | Type               | Attributes
# ----------------- | ------------------ | ---------------------------
# **`id`**          | `uuid`             | `not null, primary key`
# **`created_at`**  | `datetime`         | `not null`
# **`updated_at`**  | `datetime`         | `not null`
#

module EtlImporter
  class Channel < ApplicationRecord
  end
end
