# ## Schema Information
#
# Table name: `etl_importer_discount_plan_discount_plan_controls`
#
# ### Columns
#
# Name                                         | Type               | Attributes
# -------------------------------------------- | ------------------ | ---------------------------
# **`id`**                                     | `uuid`             | `not null, primary key`
# **`etl_importer_discount_plan_id`**          | `uuid`             |
# **`etl_importer_discount_plan_control_id`**  | `uuid`             |
# **`created_at`**                             | `datetime`         | `not null`
# **`updated_at`**                             | `datetime`         | `not null`
#
# ### Indexes
#
# * `index_etl_importer_d_p_d_p_cont_on_etl_importer_d_plan_cont_id`:
#     * **`etl_importer_discount_plan_control_id`**
# * `index_etl_importer_d_plan_d_plan_cont_on_etl_importer_d_plan_id`:
#     * **`etl_importer_discount_plan_id`**
#

module EtlImporter
  class DiscountPlanDiscountPlanControl < ApplicationRecord
    belongs_to :discount_plan, optional: true, foreign_key: :etl_importer_discount_plan_id
    belongs_to :discount_plan_control, optional: true, foreign_key: :etl_importer_discount_plan_control_id
  end
end
