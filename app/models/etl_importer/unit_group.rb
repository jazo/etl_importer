# ## Schema Information
#
# Table name: `etl_importer_unit_groups`
#
# ### Columns
#
# Name                                  | Type               | Attributes
# ------------------------------------- | ------------------ | ---------------------------
# **`price`**                           | `decimal(, )`      |
# **`group_key`**                       | `string`           |
# **`name`**                            | `string`           |
# **`available_units_count`**           | `integer`          |
# **`total_units_count`**               | `integer`          |
# **`total_non_excluded_units_count`**  | `integer`          |
# **`size`**                            | `string`           |
# **`standard_rate`**                   | `integer`          |
# **`floor`**                           | `integer`          |
# **`reduced_price`**                   | `decimal(, )`      |
# **`occupancy_percent`**               | `integer`          |
# **`area`**                            | `integer`          |
# **`length`**                          | `integer`          |
# **`width`**                           | `integer`          |
# **`height`**                          | `integer`          |
# **`due_at_move_in`**                  | `integer`          |
# **`due_at_move_in_without_fees`**     | `integer`          |
# **`due_monthly`**                     | `integer`          |
# **`attribute_description`**           | `text`             |
# **`description`**                     | `text`             |
# **`average_rent`**                    | `integer`          |
# **`active_rate_type`**                | `string`           |
# **`etl_importer_channel_rate_id`**    | `uuid`             |
# **`etl_importer_unit_type_id`**       | `uuid`             |
# **`created_at`**                      | `datetime`         | `not null`
# **`updated_at`**                      | `datetime`         | `not null`
# **`id`**                              | `string`           | `not null, primary key`
#
# ### Indexes
#
# * `index_etl_importer_unit_groups_on_etl_importer_channel_rate_id`:
#     * **`etl_importer_channel_rate_id`**
# * `index_etl_importer_unit_groups_on_etl_importer_unit_type_id`:
#     * **`etl_importer_unit_type_id`**
#

module EtlImporter
  class UnitGroup < ApplicationRecord
    belongs_to :channel_rate, optional: true, foreign_key: :etl_importer_channel_rate_id
    belongs_to :unit_type, optional: true, foreign_key: :etl_importer_unit_type_id

    has_many :unit_group_invoiceable_fees, foreign_key: :etl_importer_unit_group_id

    has_many :invoiceable_fees,
             through:     :unit_group_invoiceable_fees,
             foreign_key: :etl_importer_unit_group_id

    has_many :unit_group_unit_amenity, foreign_key: :etl_importer_unit_group_id

    has_many :unit_amenities,
             through:     :unit_group_unit_amenity,
             foreign_key: :etl_importer_unit_group_id

    has_many :unit_group_discount_plans, foreign_key: :etl_importer_unit_group_id
    has_many :discount_plans,
             through:     :unit_group_discount_plans,
             foreign_key: :etl_importer_unit_group_id
  end
end
