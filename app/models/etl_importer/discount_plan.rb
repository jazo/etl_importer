# ## Schema Information
#
# Table name: `etl_importer_discount_plans`
#
# ### Columns
#
# Name                                                 | Type               | Attributes
# ---------------------------------------------------- | ------------------ | ---------------------------
# **`id`**                                             | `uuid`             | `not null, primary key`
# **`name`**                                           | `string`           |
# **`description`**                                    | `text`             |
# **`public_description`**                             | `text`             |
# **`availability_text`**                              | `text`             |
# **`requirements_text`**                              | `text`             |
# **`kind`**                                           | `string`           |
# **`min_occupancy_required`**                         | `boolean`          |
# **`min_occupancy_months`**                           | `boolean`          |
# **`prepay_required`**                                | `boolean`          |
# **`prepay_months`**                                  | `integer`          |
# **`start_date`**                                     | `date`             |
# **`end_date`**                                       | `date`             |
# **`auto_apply`**                                     | `boolean`          |
# **`promotion_type`**                                 | `string`           |
# **`deleted`**                                        | `boolean`          |
# **`turned_on`**                                      | `boolean`          |
# **`available_for_all_facilities`**                   | `boolean`          |
# **`round_to_nearest`**                               | `string`           |
# **`hide_from_website`**                              | `boolean`          |
# **`move_in_only`**                                   | `boolean`          |
# **`existing_tenant_only`**                           | `boolean`          |
# **`priority`**                                       | `integer`          |
# **`role_permission`**                                | `string`           |
# **`etl_importer_always_discount_plan_discount_id`**  | `uuid`             |
# **`etl_importer_tenant_account_kind_id`**            | `uuid`             |
# **`created_at`**                                     | `datetime`         | `not null`
# **`updated_at`**                                     | `datetime`         | `not null`
#
# ### Indexes
#
# * `index_etl_importer_d_plan_on_etl_importer_always_dis_id`:
#     * **`etl_importer_always_discount_plan_discount_id`**
# * `index_etl_importer_d_plan_on_etl_importer_tenant_acco_kind_id`:
#     * **`etl_importer_tenant_account_kind_id`**
#

module EtlImporter
  class DiscountPlan < ApplicationRecord
    belongs_to :always_discount_plan_discount,
               optional:    true,
               foreign_key: :etl_importer_always_discount_plan_discount_id
    belongs_to :tenant_account_kind,
               optional:    true,
               foreign_key: :etl_importer_tenant_account_kind_id
    belongs_to :unit_group,
               optional:    true,
               foreign_key: :etl_importer_unit_group_id

    has_many :discount_plan_discounts,
             foreign_key: :etl_importer_discount_plan_id

    has_many :discount_plan_discount_plan_control,
             foreign_key: :etl_importer_discount_plan_id

    has_many :discount_plan_controls,
             through:     :discount_plan_discount_plan_control,
             foreign_key: :etl_importer_discount_plan_id

    has_many :discount_plan_api_associations,
             foreign_key: :etl_importer_discount_plan_id

    has_many :api_associations,
             through:     :discount_plan_api_associations,
             foreign_key: :etl_importer_discount_plan_id

    has_many :discount_plan_client_applications,
             foreign_key: :etl_importer_discount_plan_id

    has_many :client_applications,
             through:     :discount_plan_client_applications,
             foreign_key: :etl_importer_discount_plan_id

    has_many :discount_plan_facilities,
             foreign_key: :etl_importer_discount_plan_id

    has_many :facilities,
             through:     :discount_plan_facilities,
             foreign_key: :etl_importer_discount_plan_id
  end
end
