# ## Schema Information
#
# Table name: `etl_importer_unit_group_discount_plans`
#
# ### Columns
#
# Name                                 | Type               | Attributes
# ------------------------------------ | ------------------ | ---------------------------
# **`id`**                             | `uuid`             | `not null, primary key`
# **`etl_importer_unit_group_id`**     | `string`           |
# **`etl_importer_discount_plan_id`**  | `uuid`             |
# **`created_at`**                     | `datetime`         | `not null`
# **`updated_at`**                     | `datetime`         | `not null`
#
# ### Indexes
#
# * `index_etl_importer_un_gro_disc_plans_on_etl_importer_di_plan_id`:
#     * **`etl_importer_discount_plan_id`**
# * `index_etl_importer_un_gro_disc_plans_on_etl_importer_un_gro_id`:
#     * **`etl_importer_unit_group_id`**
#

module EtlImporter
  class UnitGroupDiscountPlan < ApplicationRecord
    belongs_to :unit_group, optional: true, foreign_key: :etl_importer_unit_group_id
    belongs_to :discount_plan, optional: true, foreign_key: :etl_importer_discount_plan_id
  end
end
