# ## Schema Information
#
# Table name: `etl_importer_always_discount_plan_discounts`
#
# ### Columns
#
# Name              | Type               | Attributes
# ----------------- | ------------------ | ---------------------------
# **`id`**          | `uuid`             | `not null, primary key`
# **`created_at`**  | `datetime`         | `not null`
# **`updated_at`**  | `datetime`         | `not null`
#

module EtlImporter
  class AlwaysDiscountPlanDiscount < ApplicationRecord
  end
end
