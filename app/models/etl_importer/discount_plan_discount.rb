# ## Schema Information
#
# Table name: `etl_importer_discount_plan_discounts`
#
# ### Columns
#
# Name                                 | Type               | Attributes
# ------------------------------------ | ------------------ | ---------------------------
# **`id`**                             | `uuid`             | `not null, primary key`
# **`etl_importer_discount_plan_id`**  | `uuid`             |
# **`discount_type`**                  | `string`           |
# **`month_number`**                   | `integer`          |
# **`amount`**                         | `decimal(, )`      |
# **`minimum_amount`**                 | `decimal(, )`      |
# **`maximum_amount`**                 | `decimal(, )`      |
# **`created_at`**                     | `datetime`         | `not null`
# **`updated_at`**                     | `datetime`         | `not null`
#
# ### Indexes
#
# * `index_etl_importer_d_plan_discounts_on_etl_importer_d_plan_id`:
#     * **`etl_importer_discount_plan_id`**
#

module EtlImporter
  class DiscountPlanDiscount < ApplicationRecord
    belongs_to :discount_plan, optional: true, foreign_key: :etl_importer_discount_plan_id
  end
end
