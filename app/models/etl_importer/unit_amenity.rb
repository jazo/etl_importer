# ## Schema Information
#
# Table name: `etl_importer_unit_amenities`
#
# ### Columns
#
# Name                                         | Type               | Attributes
# -------------------------------------------- | ------------------ | ---------------------------
# **`id`**                                     | `uuid`             | `not null, primary key`
# **`name`**                                   | `string`           |
# **`short_code`**                             | `string`           |
# **`show_in_sales_center_filter_dropdown`**   | `boolean`          |
# **`created_at`**                             | `datetime`         | `not null`
# **`updated_at`**                             | `datetime`         | `not null`
# **`etl_importer_discount_plan_control_id`**  | `uuid`             |
#
# ### Indexes
#
# * `index_etl_importer_unit_ameniti_on_etl_importer_di_plan_cont_id`:
#     * **`etl_importer_discount_plan_control_id`**
#

module EtlImporter
  class UnitAmenity < ApplicationRecord
    belongs_to :discount_plan_control, optional: true, foreign_key: :etl_importer_discount_plan_control_id
  end
end
