# ## Schema Information
#
# Table name: `etl_importer_units`
#
# ### Columns
#
# Name                                | Type               | Attributes
# ----------------------------------- | ------------------ | ---------------------------
# **`id`**                            | `uuid`             | `not null, primary key`
# **`price`**                         | `decimal(, )`      |
# **`name`**                          | `string`           |
# **`description`**                   | `text`             |
# **`width`**                         | `integer`          |
# **`length`**                        | `integer`          |
# **`height`**                        | `integer`          |
# **`door_height`**                   | `integer`          |
# **`door_width`**                    | `integer`          |
# **`door_type`**                     | `string`           |
# **`access_type`**                   | `string`           |
# **`floor`**                         | `string`           |
# **`size`**                          | `string`           |
# **`area`**                          | `integer`          |
# **`standard_rate`**                 | `integer`          |
# **`managed_rate`**                  | `string`           |
# **`available_for_move_in`**         | `boolean`          |
# **`rentable`**                      | `boolean`          |
# **`status`**                        | `string`           |
# **`payment_status`**                | `string`           |
# **`current_ledger_id`**             | `string`           |
# **`current_tenant_id`**             | `string`           |
# **`combination_lock_number`**       | `string`           |
# **`attribute_description`**         | `string`           |
# **`deleted`**                       | `boolean`          |
# **`damaged`**                       | `boolean`          |
# **`complimentary`**                 | `boolean`          |
# **`etl_importer_channel_rate_id`**  | `uuid`             |
# **`created_at`**                    | `datetime`         | `not null`
# **`updated_at`**                    | `datetime`         | `not null`
#
# ### Indexes
#
# * `index_etl_importer_units_on_etl_importer_channel_rate_id`:
#     * **`etl_importer_channel_rate_id`**
#

module EtlImporter
  class Unit < ApplicationRecord
    belongs_to :channel_rate, optional: true
  end
end
