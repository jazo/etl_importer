# ## Schema Information
#
# Table name: `etl_importer_unit_group_invoiceable_fees`
#
# ### Columns
#
# Name                                    | Type               | Attributes
# --------------------------------------- | ------------------ | ---------------------------
# **`id`**                                | `uuid`             | `not null, primary key`
# **`etl_importer_unit_group_id`**        | `string`           |
# **`etl_importer_invoiceable_fees_id`**  | `uuid`             |
# **`created_at`**                        | `datetime`         | `not null`
# **`updated_at`**                        | `datetime`         | `not null`
#
# ### Indexes
#
# * `index_etl_importer_un_gpo_inv_fees_on_etl_importer_inv_fees_id`:
#     * **`etl_importer_invoiceable_fees_id`**
# * `index_etl_importer_un_gpo_invo_fees_on_etl_importer_un_gpo_id`:
#     * **`etl_importer_unit_group_id`**
#

module EtlImporter
  class UnitGroupInvoiceableFee < ApplicationRecord
    belongs_to :unit_group, optional: true, foreign_key: :etl_importer_unit_group_id
    belongs_to :invoiceable_fee, optional: true, foreign_key: :etl_importer_invoiceable_fees_id
  end
end
