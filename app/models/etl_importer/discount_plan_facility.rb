# ## Schema Information
#
# Table name: `etl_importer_discount_plan_facilities`
#
# ### Columns
#
# Name                                 | Type               | Attributes
# ------------------------------------ | ------------------ | ---------------------------
# **`id`**                             | `uuid`             | `not null, primary key`
# **`etl_importer_discount_plan_id`**  | `uuid`             |
# **`etl_importer_facility_id`**       | `uuid`             |
# **`created_at`**                     | `datetime`         | `not null`
# **`updated_at`**                     | `datetime`         | `not null`
#
# ### Indexes
#
# * `index_etl_importer_d_plan_facilites_on_etl_importer_facility_id`:
#     * **`etl_importer_facility_id`**
# * `index_etl_importer_d_plan_facilities_on_etl_importer_d_plan_id`:
#     * **`etl_importer_discount_plan_id`**
#

module EtlImporter
  class DiscountPlanFacility < ApplicationRecord
    belongs_to :discount_plan, optional: true, foreign_key: :etl_importer_discount_plan_id
    belongs_to :facility, optional: true, foreign_key: :etl_importer_facility_id
  end
end
