# ## Schema Information
#
# Table name: `etl_importer_facilities`
#
# ### Columns
#
# Name              | Type               | Attributes
# ----------------- | ------------------ | ---------------------------
# **`id`**          | `uuid`             | `not null, primary key`
# **`created_at`**  | `datetime`         | `not null`
# **`updated_at`**  | `datetime`         | `not null`
#

module EtlImporter
  class Facility < ApplicationRecord
  end
end
