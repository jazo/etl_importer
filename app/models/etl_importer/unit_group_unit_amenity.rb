# ## Schema Information
#
# Table name: `etl_importer_unit_group_unit_amenities`
#
# ### Columns
#
# Name                                  | Type               | Attributes
# ------------------------------------- | ------------------ | ---------------------------
# **`id`**                              | `uuid`             | `not null, primary key`
# **`etl_importer_unit_amenities_id`**  | `uuid`             |
# **`created_at`**                      | `datetime`         | `not null`
# **`updated_at`**                      | `datetime`         | `not null`
# **`etl_importer_unit_group_id`**      | `string`           |
#
# ### Indexes
#
# * `index_etl_importer_uni_group_un_amen_on_etl_importer_un_gop_id`:
#     * **`etl_importer_unit_group_id`**
# * `index_etl_importer_unit_gop_un_amen_on_etl_importer_un_amen_id`:
#     * **`etl_importer_unit_amenities_id`**
#

module EtlImporter
  class UnitGroupUnitAmenity < ApplicationRecord
    belongs_to :unit_group, optional: true, foreign_key: :etl_importer_unit_group_id
    belongs_to :unit_amenity, optional: true, foreign_key: :etl_importer_unit_amenities_id
  end
end
