# ## Schema Information
#
# Table name: `etl_importer_client_applications`
#
# ### Columns
#
# Name                    | Type               | Attributes
# ----------------------- | ------------------ | ---------------------------
# **`id`**                | `uuid`             | `not null, primary key`
# **`name`**              | `string`           |
# **`channel_rates_on`**  | `boolean`          |
# **`internal`**          | `boolean`          |
# **`created_at`**        | `datetime`         | `not null`
# **`updated_at`**        | `datetime`         | `not null`
#

module EtlImporter
  class ClientApplication < ApplicationRecord
  end
end
