# ## Schema Information
#
# Table name: `etl_importer_discount_plan_client_applications`
#
# ### Columns
#
# Name                                      | Type               | Attributes
# ----------------------------------------- | ------------------ | ---------------------------
# **`id`**                                  | `uuid`             | `not null, primary key`
# **`etl_importer_discount_plan_id`**       | `uuid`             |
# **`etl_importer_client_application_id`**  | `uuid`             |
# **`created_at`**                          | `datetime`         | `not null`
# **`updated_at`**                          | `datetime`         | `not null`
#
# ### Indexes
#
# * `index_etl_importer_d_plan_client_app_on_etl_importer_d_plan_id`:
#     * **`etl_importer_discount_plan_id`**
# * `index_etl_importer_d_pln_cli_apps_on_etl_importer_client_app_id`:
#     * **`etl_importer_client_application_id`**
#

module EtlImporter
  class DiscountPlanClientApplication < ApplicationRecord
    belongs_to :discount_plan, optional: true, foreign_key: :etl_importer_discount_plan_id
    belongs_to :client_application, optional: true, foreign_key: :etl_importer_client_application_id
  end
end
