# ## Schema Information
#
# Table name: `etl_importer_unit_types`
#
# ### Columns
#
# Name                         | Type               | Attributes
# ---------------------------- | ------------------ | ---------------------------
# **`id`**                     | `uuid`             | `not null, primary key`
# **`name`**                   | `string`           |
# **`deleted`**                | `boolean`          |
# **`internal_account_code`**  | `string`           |
# **`code_and_description`**   | `text`             |
# **`created_at`**             | `datetime`         | `not null`
# **`updated_at`**             | `datetime`         | `not null`
#

module EtlImporter
  class UnitType < ApplicationRecord
  end
end
