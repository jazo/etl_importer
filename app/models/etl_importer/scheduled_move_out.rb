# ## Schema Information
#
# Table name: `etl_importer_scheduled_move_outs`
#
# ### Columns
#
# Name              | Type               | Attributes
# ----------------- | ------------------ | ---------------------------
# **`id`**          | `uuid`             | `not null, primary key`
# **`created_at`**  | `datetime`         | `not null`
# **`updated_at`**  | `datetime`         | `not null`
#

module EtlImporter
  class ScheduledMoveOut < ApplicationRecord
  end
end
